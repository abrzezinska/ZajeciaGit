using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float jumpForce;

    private const float InterpolationMultiplier = 10f;

    private void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.W)){
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.forward * moveSpeed, Time.deltaTime * InterpolationMultiplier);
        }
        if(Input.GetKey(KeyCode.S)){
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.forward * -moveSpeed, Time.deltaTime * InterpolationMultiplier);
        }
        if(Input.GetKey(KeyCode.A)){
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.right * -moveSpeed, Time.deltaTime * InterpolationMultiplier);
        }
        if(Input.GetKey(KeyCode.D)){
            transform.position = Vector3.Lerp(transform.position, transform.position + transform.right * moveSpeed, Time.deltaTime * InterpolationMultiplier);
        }
        if(Input.GetKeyUp(KeyCode.Space)){
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }
}
